// DiagnosticRunnerTests.cpp : Defines the entry point for the console application.
//
#pragma comment(lib, "wbemuuid.lib")
#pragma comment(lib, "comsuppwd.lib")

#include "stdafx.h"
#include "gtest\gtest.h"

#include "ConfigReaderTest.h"
#include "DiagnosticTest.h"
#include "TransferTest.h"


int main(int argc, char* argv[])
{
	testing::InitGoogleTest(&argc, argv);

	RUN_ALL_TESTS();
	std::getchar();
}

