#pragma once

#include "gtest\gtest.h"
#include "DiagnosticRunner.h"

using namespace std;

// test to see if values are returned from WMI
TEST(diagnostic_test, test_all)
{
	try
	{
		DiagnosticRunner runner;
		auto memUsage = runner.getMemUsage();
		if (memUsage.first == 0.0 || memUsage.second == 0.0)
			FAIL();
		auto cpuUsage = runner.getCpuUsage();
		if (cpuUsage == 0.0)
			FAIL();
		auto numProcess = runner.getProcDetailsAsString();
		if (numProcess == "")
			FAIL();
	}
	catch (const exception& e)
	{
		FAIL();
	}
}
