#pragma once
#include "gtest\gtest.h"
#include "Reporter.h"

// test wrong parameters
TEST(transfer_test, transfer_failure)
{
	bool result = true;
	try 
	{
		result = Reporter::sendDiagnosis("", "", "", 0,0 , 0, "");
		if (result)
			FAIL();
	}
	catch (const exception& e)
	{
		;
	}
}
