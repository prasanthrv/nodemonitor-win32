#pragma once

#include "gtest\gtest.h"
#include "boost\filesystem.hpp"
#include "ConfigReader.h"

// test login parsing
TEST(test_parser, parse_login)
{
	string relPath = "DiagnosticRunner\\config\\config.xml";
	auto path = boost::filesystem::current_path().parent_path().parent_path();
	path = path /= relPath;
	auto pathStr = path.string();

	auto clientCred = ConfigReader::readClientCredentials(pathStr);

	EXPECT_TRUE(clientCred.username == "cl1");
	EXPECT_TRUE(clientCred.password == "cl1pass");
}

// test url parsing
TEST( test_parser, parse_url)
{
	string relPath = "DiagnosticRunner\\config\\config.xml";
	auto path = boost::filesystem::current_path().parent_path().parent_path() /= relPath;
	auto pathStr = path.string();

	auto url = ConfigReader::readReportURL(path.string());

	EXPECT_TRUE(url.host == "localhost");
	EXPECT_TRUE(url.path == "/api/diagnostics");
	EXPECT_TRUE(url.port == 3000);
}

//test with file_not_found
TEST(test_parse, parse_not_found)
{
	auto path = boost::filesystem::current_path();
	try
	{
		// should throw exception
		auto url = ConfigReader::readReportURL(path.string());
		FAIL();
	}
	catch (const exception& e)
	{
		;
	}
}

