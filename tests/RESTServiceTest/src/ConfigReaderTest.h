#pragma once

#include "gtest\gtest.h"
#include "ConfigReader.h"
#include "boost\filesystem.hpp"

// test init parsing
TEST(test_parser, init_parse)
{
	string relPath = "RESTService\\config\\config.xml";
	auto path = boost::filesystem::current_path().parent_path()
		.parent_path() /= relPath;
	auto pathStr = path.string();

	ConfigReader reader;

	auto clientData = reader.readClientData(pathStr);
}

// test parsing
TEST(test_parser, test_parse)
{
	string relPath = "RESTService\\config\\config.xml";
	auto path = boost::filesystem::current_path().parent_path()
		.parent_path() /= relPath;
	auto pathStr = path.string();

	ConfigReader reader;

	auto clientData = reader.readClientData(pathStr);

	auto client1 = clientData.at("cl1");

	EXPECT_TRUE(client1.cDetails.password == "cl1pass");
	EXPECT_TRUE(client1.cDetails.username == "cl1");
	EXPECT_TRUE(client1.cDetails.mail == "xx@xx.com");

	EXPECT_TRUE(client1.cpuCapacity == 0.2);
	EXPECT_TRUE(client1.memoryCapacity == 0.5);
	EXPECT_TRUE(client1.processCapacity == 50);

	auto client2 = clientData.at("cl2");

	EXPECT_TRUE(client2.cDetails.password == "cl2pass");
	EXPECT_TRUE(client2.cDetails.username == "cl2");
	EXPECT_TRUE(client2.cDetails.mail == "yy@yy.com");

	EXPECT_TRUE(client2.cpuCapacity == 0.1);
	EXPECT_TRUE(client2.memoryCapacity == 0.3);
	EXPECT_TRUE(client2.processCapacity == 10);
	
}
