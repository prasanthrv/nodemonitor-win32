#pragma once

#include "gtest\gtest.h"
#include "spdlog\spdlog.h"
#include "boost\filesystem.hpp"

using namespace std;

// test one off logging
TEST(test_log, init_logging)
{
	
	string fileName = "clt.log";
	auto path = boost::filesystem::current_path().parent_path().parent_path()
		/= "RESTService\\logs";
	path /= fileName;
	auto pathStr = path.string();
	
	auto file_logger = spdlog::basic_logger_st("clt", pathStr);
	file_logger->info("Logging is working");

	spdlog::drop("clt");
	file_logger.reset();

	EXPECT_TRUE(boost::filesystem::exists(path));
	boost::filesystem::remove(path);
}

// test rotational logging
TEST(test_log, rotate_logging)
{

	string fileName = "rotateLog";
	const auto path = boost::filesystem::current_path().parent_path().parent_path()
		/= "RESTService\\logs";
	auto orgPath = path / fileName;

	string fileName1 = "rotateLog.1.txt";
	auto path1 = path / fileName1;

	string fileName2 = "rotateLog.2.txt";
	auto path2 = path / fileName2;

	string fileName3 = "rotateLog.3.txt";
	auto path3 = path / fileName3;

	auto pathStr = orgPath.string();

	auto rotateLogger = spdlog::rotating_logger_st("rot1", pathStr
	, 1048576 * 2, 10);
	rotateLogger->set_pattern("[%T] %l : %v");
	for (long i = 0; i < 300000; i++)
		rotateLogger->warn("Test");

	spdlog::drop_all();
	rotateLogger.reset();
	
	EXPECT_TRUE(boost::filesystem::exists(path1));
	EXPECT_TRUE(boost::filesystem::exists(path2));
	EXPECT_TRUE(boost::filesystem::exists(path3));

	boost::filesystem::remove(path / "rotateLog.txt");
	boost::filesystem::remove(path1);
	boost::filesystem::remove(path2);
	boost::filesystem::remove(path3);
}

