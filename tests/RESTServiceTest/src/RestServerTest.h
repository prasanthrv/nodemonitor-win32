#pragma once

#include <thread>
#include "gtest\gtest.h"
#include "RestServer.h"


// test server initialization
TEST(test_rest_server, init_server)
{

	try 
	{
		RestServer rServer("localhost", "api", 3000);
	}
	catch (const std::exception& e)
	{
		std::cout << e.what() << std::endl;
		FAIL();
	}
	EXPECT_TRUE(true);
}

// handler for testing
class TestHandler : public APIHandler
{
	pair<bool,string> process(unordered_map<string, string> params) 
	{
		cout << "Hi there!!";
		cout << endl << "Map values: " << endl;
		for (pair<string, string> param : params) {
			cout << param.first << " : " << param.second << endl;
		}
		return make_pair(true, "");
	}
};


// test server running
TEST(test_rest_server, run_server)
{
	try
	{
		// custom test config, run for 3 seconds
		RestServer rServer("localhost", "api", 3000, 3);
		// register API
		shared_ptr<APIHandler> fnPtr;
		auto testHandler = new TestHandler();
		fnPtr.reset(&dynamic_cast<APIHandler&>(*testHandler));
		rServer.addAPI(web::http::methods::POST, 
			"/test", fnPtr);

		rServer.start();
	}
	catch (const std::exception& e)
	{
		std::cout << e.what() << std::endl;
		FAIL();
	}

	EXPECT_TRUE(true);
	
}

