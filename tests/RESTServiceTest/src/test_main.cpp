#include <iostream>

#include "ConfigReaderTest.h"
#include "LoggingTest.h"
#include "RestServerTest.h"

int main(int argc, char* argv[])
{
	testing::InitGoogleTest(&argc, argv);

	RUN_ALL_TESTS();
	std::getchar();
}