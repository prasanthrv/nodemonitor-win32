#include "stdafx.h"
#include "..\include\ClientConfig.h"


ClientConfig::ClientConfig(Credentials cDetails, double memoryCapacity = 0.0,
	double cpuCapacity = 0.0, 
	double processCapacity = 0.0): cDetails(cDetails), memoryCapacity(memoryCapacity),
	cpuCapacity(cpuCapacity), processCapacity(processCapacity)
{
	// are there any alert parameters ?
	if (memoryCapacity == 0.0 && cpuCapacity == 0.0 && processCapacity == 0.0)
	{
		string exceptMessage = "No alerts are set for " + cDetails.username;
		throw new exception(exceptMessage.c_str());
	}

	// check if any alert parameters are invalid
	if (memoryCapacity < 0 || memoryCapacity > 1 || cpuCapacity < 0 || cpuCapacity > 1)
	{
		string exceptMessage = "Alerts out of range for " + cDetails.username;
		throw new exception(exceptMessage.c_str());
	}
}

ClientConfig::ClientConfig(const ClientConfig & client):
	cDetails(client.cDetails),
	memoryCapacity(client.memoryCapacity), cpuCapacity(client.cpuCapacity),
	processCapacity(client.processCapacity)
{
}


ClientConfig::~ClientConfig()
{
}

Credentials ClientConfig::credentials()
{
	return cDetails;
}

