#include "stdafx.h"
#include "curl\curl.h"
#include "boost\filesystem.hpp"
#include "csmtp\CSmtp.h"
#include "..\include\Mailer.h"



namespace Mailer
{
	bool sendMail(const string & from, const string & pass, 
		const string& url, const int port,
		const string & to, const string& authType, const string & subject, 
		const string & message)
	{
		bool mailSent = true;
		try 
		{
			CSmtp mail;

			mail.SetSMTPServer(url.c_str(), port);
			if (authType == "TLS")
				mail.SetSecurityType(USE_TLS);
			else if(authType == "SSL")
				mail.SetSecurityType(USE_SSL);

			mail.SetLogin(from.c_str());
			mail.SetPassword(pass.c_str());

			mail.SetSenderMail(from.c_str());
			mail.SetSubject(subject.c_str());
			mail.AddRecipient(to.c_str());
			mail.SetXPriority(XPRIORITY_NORMAL);

			mail.AddMsgLine(message.c_str());
			mail.Send();
		}
		catch (const ECSmtp& e)
		{
			mailSent = false;
			cerr << "Error sending mail: " << e.GetErrorText() << endl;
		}
		return mailSent;
	}
}