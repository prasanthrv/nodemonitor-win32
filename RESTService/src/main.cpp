#include "stdafx.h"
#include "../include/RestServer.h"
#include "ClientHandler.h"
#include "ConfigReader.h"
#include "utils.h"

int main(int argc, char* argv[])
{

	const std::string host = "localhost";
	const std::string path = "api";
	const int port = 3000;

	try
	{
		// read client config from xml
		string configPath = getConfigPath();
		ConfigReader reader;
		auto clientData = reader.readClientData(configPath);

		// initalize server
		RestServer server(host, path, port);
		// register API
		shared_ptr<APIHandler> fnPtr;
		auto diagnosticHandler = new ClientHandler(clientData);
		fnPtr.reset(&dynamic_cast<APIHandler&>(*diagnosticHandler));
		server.addAPI(web::http::methods::POST, "/diagnostics",
					fnPtr);
		// run server
		server.start();
	}
	catch (std::exception& e)
	{
		cout << e.what() << endl;
		return -1;

	}
	
	return 0;
}