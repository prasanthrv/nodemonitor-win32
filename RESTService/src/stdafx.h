#pragma once

#include <iostream>
#include <unordered_map>

#include <cpprest/http_listener.h>
#include <cpprest/uri.h>
#include <boost/filesystem.hpp>

using namespace web::http::experimental::listener;
using namespace web::http;
using namespace utility;
using namespace utility::conversions;
using namespace std;


typedef std::string UTF8String;
typedef std::wstring UTF16String;


