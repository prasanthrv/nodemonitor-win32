#pragma once
#include "stdafx.h"
#include "APIHandler.h"
#include "ClientConfig.h"
#include "spdlog\spdlog.h"
#include "boost\filesystem.hpp"
#include "Mailer.h"
#include "ConfigReader.h"
#include "utils.h"
#include <iomanip>

// don't fail fast, catch exceptions
// set to 0 to fail fast with exception
#define NO_FAST_FAIL 1

// API Handler for the /diagnostics API call
// made by client application
class ClientHandler : public APIHandler
{
	// map of (name,config) for all clients
	unordered_map<string, ClientConfig> clientData;

public:
	ClientHandler(unordered_map<string, ClientConfig>& config) : clientData(config)
	{}

	// process the paramaters passed to REST API
	pair<bool,string> process(unordered_map<string, string> params) 
	{
			string errMessage = "Success";
#if NO_FAST_FAIL 
		try
		{
#endif
			string username = params.at("login");
			string password = params.at("pass");

			ClientConfig config = clientData.at(username);

			// check password
			if (password == config.cDetails.password)
			{
				// convert params
				const long usedMem = strtol(params.at("used_mem").c_str(),nullptr,10);
				const long freeMem = strtol(params.at("free_mem").c_str(), nullptr,10);
				const double cpuUsage = strtod(params.at("cpu_usage").c_str(), nullptr);
				int procCpy = 0;
				string procDetails = params.at("process_details");

				// get log file path, log file is login_name
				string logfileName = username;
				auto path = boost::filesystem::current_path() /= "logs";
				path = path / username;

				// rotational logger capped at 2mb each
				auto rotateLogger = spdlog::rotating_logger_st(username, path.string()
					, 1048576 * 2, 100);
				rotateLogger->set_pattern("[%T] %l : %v");
				auto processList = split_string(procDetails, ";");
				procCpy = processList.size() - 2;
				string sep = "\t\t\t";
				stringstream logText;
				logText << endl << "Report values: " << endl
						<< sep << "Used Memory = " << usedMem    << " MB" << endl
						<< sep << "Free Memory = " << freeMem    << " MB" << endl
						<< sep << "Cpu Usage = "   << cpuUsage * 100  << "%"  << endl
						<< sep << "Processes = "   << procCpy    << endl;


				// dump process list and details
				logText << "Processes: " << endl;
				for (auto process : processList)
				{
					auto procMetrics = split_string(process, ",");
					if (procMetrics.size() == 3)
						logText << sep << sep << setw(40)   << procMetrics[0] << "\t"
							    << setw(20)   << "Memory: " << procMetrics[1] << " MB" << "\t" 
							    << setw(10)   << "CPU: "    << procMetrics[2] << " %"  << endl;

				}
				

				// log at [INFO]
				rotateLogger->info(logText.str());
				rotateLogger->flush();
				spdlog::drop(username);



				// invalid values
				if (usedMem == 0 || freeMem == 0 || cpuUsage > 1 
					|| cpuUsage < 0|| procCpy == 0) 
					throw exception("Invalid parameter values.");

				long totalMem = freeMem + usedMem;
				double memUsage = static_cast<double>(usedMem) / static_cast<double>(totalMem);

				// threshold exceeded
				if (memUsage > config.memoryCapacity || cpuUsage > config.cpuCapacity
					|| procCpy > config.processCapacity)
				{
					// output on console
					// for debug and video demonstration
					stringstream  details;
					details << "Threshold exceeded for client " << username << endl << endl;
					if (memUsage > config.memoryCapacity)
						details << "Memory capcacity exceeded, normal: " << config.memoryCapacity
						<< " , current: " << memUsage << endl;
					if (cpuUsage > config.cpuCapacity)
						details << "CPU capcacity exceeded, normal: " << config.cpuCapacity
						<< " , current: " << cpuUsage << endl;
					if (procCpy > config.processCapacity)
						details << "Process capcacity exceeded, normal: " << config.processCapacity
						<< " , current: " << procCpy << endl;
					//cerr << details.str();

					// send email
					MailConfig mailConfig = ConfigReader::readMailConfig(getConfigPath());
					string subject = "Machine " + username + " exceeded threshold";
					Mailer::sendMail(mailConfig.login, mailConfig.password,
						mailConfig.url, mailConfig.port, config.cDetails.mail, mailConfig.authType,
						subject, details.str());
				}

				
			}
			else
				errMessage = "Invalid login";


#if NO_FAST_FAIL
		}
		catch (const std::out_of_range& e)
		{
			errMessage = "Parmeters incorrectly sent or config does not exist";
			cerr << errMessage << endl;
			return make_pair(false,errMessage);
		}
		catch (const std::exception& e)
		{
			errMessage = e.what();
			cerr << errMessage << endl;
			return make_pair(false,errMessage);
		}
#endif
		return make_pair(true,errMessage);
	}
};


