#include "stdafx.h"
#include "..\include\ConfigReader.h"

bool ConfigReader::loadXML(const string& path, pugi::xml_document& doc) 
{
	pugi::xml_parse_result result = doc.load_file(path.c_str());

	switch (result.status)
	{
		case pugi::xml_parse_status::status_file_not_found:
		{
			string parseErrorMessage = "File " + path + " not found";
			throw exception(parseErrorMessage.c_str());
			return false;
		}
		case pugi::xml_parse_status::status_ok: 
		{
			return result;
		}
		default:
		{
			string parseErrorMessage = "Error parsing XML file " + path;
			throw exception(parseErrorMessage.c_str());
			return false;
		}
	}
}

unordered_map<string,ClientConfig> ConfigReader::readClientData(const string & path) 
{
	unordered_map<string, ClientConfig> clients;
	pugi::xml_document doc;

	if (loadXML(path, doc))
	{
		//<clients>
		auto clientList = doc.first_child().child("clients");

		if (clientList != nullptr)
		{
			// <client>
			for (auto clientData: clientList.children())
			{
				// < ... login=... >
				string name = clientData.attribute("login").value();
				// < ... pass=... >
				string password = clientData.attribute("pass").value();
				// < ... mail=... >
				string mail = clientData.attribute("mail").value();
				Credentials cred(name, password, mail);

				double memCpy = 0.0, cpuCpy = 0.0, prsCpy = 0.0;

				for (auto alert : clientData.children())
				{
					//<... type= ... limit = ... >
					auto alertType = string(alert.attribute("type").value());
					auto alertLimit = string(alert.attribute("limit").value());

					if (alert.attribute("type") && !alertLimit.empty())
					{
						// convert types
						if      (alertType == "memory")
							memCpy = strtod(alertLimit.c_str(),nullptr) / 100.00;
						else if (alertType == "cpu")
							cpuCpy = strtod(alertLimit.c_str(),nullptr) / 100.00;
						else if (alertType == "processes")
							prsCpy = strtol(alertLimit.c_str(),nullptr,10);
					}
				}

				ClientConfig client(cred, memCpy, cpuCpy, prsCpy);
				clients.insert(std::make_pair(name,client));

			}
		}
	}
	return clients;
}

MailConfig ConfigReader::readMailConfig(const string & path)
{
	pugi::xml_document doc;

	if (loadXML(path, doc))
	{
		//<mail>
		auto mail = doc.first_child().child("mail");

		//<login>
		string login = mail.child("login").text().as_string();
		//<pass>
		string pass  = mail.child("pass").text().as_string();
		//<url>
		string url   = mail.child("url").text().as_string();
		//<port>
		int    port  = mail.child("port").text().as_int();
		//<auth>
		string auth   = mail.child("auth").text().as_string();

		return MailConfig(login, pass, url, auth, port);
	}
	throw exception("Could not read mail info from config xml");
}
