#include "stdafx.h"
#include "../include/RestServer.h"
#include "utils.h"
//#include <pplx\pplx.h>

auto RestServer::parseBody(const std::string & body)
{
	// ..&&..
	auto paramPairs = split_string(body, "&&");
	unordered_map<string, string> params;

	// param=type
	for (auto paramsRaw : paramPairs) {
		auto paramPair = split_string(paramsRaw, "=");
		if (paramPair.size() == 2)
			params[paramPair[0]] = paramPair[1];
	}
	return params;
}

auto RestServer::getApiHandlers(const method method)
{
	/*
	if (method == methods::GET)
		return apiGET;
	else if (method == methods::POST)
		return apiPOST;
	else if (method == methods::DEL)
		return apiDELETE;
	else if (method == methods::PUT)
		return apiPUT;
	*/
		return apiPOST;
}

RestServer::RestServer(const UTF8String & host, const UTF8String & relPath, 
	const int port, int duration): duration(duration)
{
	// build url
	uri_builder builder;
	builder.set_scheme(U("http"));
	builder.set_host(conversions::to_string_t(host), true);
	builder.set_port(port);
	builder.set_path(conversions::to_string_t(relPath), true);
	
	// init listener
	listener = http_listener(builder.to_uri());
}

RestServer::~RestServer()
{
	listener.close();
}

bool RestServer::addAPI(const method methodType, 
	const UTF8String & resourceURL, shared_ptr<APIHandler> handler)
{

	apiPOST.insert(make_pair(resourceURL, handler));
	return false;
}

void RestServer::start()
{
	try
	{
		// add handler
		listener.support(std::bind(&RestServer::routeHandler,this,
			std::placeholders::_1));
		// open listener
		listener.open().wait();
			
		// sleep for (duration)
		if (duration != 0)
		{
			try
			{
				this_thread::sleep_for(std::chrono::seconds(duration));
			}
			catch (const exception& e)
			{
				listener.close();
			}
		}
		else
			// sleep indefinitely
			while (true)
			{
				try
				{
					this_thread::sleep_for(std::chrono::seconds(60));
				}
				catch (const exception& e)
				{
					listener.close();
				}

			}
	}
	catch (const std::exception& e)
	{
		std::cout << "Error: " << e.what() << std::endl;
	}
}



void RestServer::routeHandler(http_request req)
{
	/* get api path*/
	string route = utility::conversions::to_utf8string(req.relative_uri().path());
	auto rt = req.relative_uri().path();
	string yt = utility::conversions::to_utf8string(rt);

	//std::cout << "Request made to uri: " << 
	//	route << std::endl;
	pair<bool,string> processedResult;

	auto meth = req.method();
	req.extract_utf8string()
		.then([this, route, &processedResult,&req](pplx::task<std::string> body) {

		std::string bodyText = body.get();
		//std::cout << "Request body: " << body.get() << std::endl;

		/*get key-value params*/
		auto params = parseBody(bodyText);

		try
		{
			/* run handler on params*/
			if (!apiPOST.empty())
			{
				auto handler = apiPOST.at(route);
				processedResult = handler->process(params);
			}
		}
		catch (const exception& e)
		{
			cout << "Exception running handler  for" << route << " " << e.what() << endl;
		}

	}).get();
	// look at result
	if (processedResult.first)
		req.reply(status_codes::OK, "API POST received and processed \n" + processedResult.second );
	else
		req.reply(status_codes::ExpectationFailed , processedResult.second);
				
}





