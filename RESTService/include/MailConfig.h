#pragma once
#include "../src/stdafx.h"

// stores the mail information needed 
// by the server for SMTP
struct MailConfig
{
	const string login;
	const string password;
	// e.g. smtp.gmail.com
	const string url;
	// SSL or TLS
	const string authType;
	const int port;

	MailConfig(string& login, string& password, 
		string& url, string& authType, int port)
		: login(login), password(password), url(url),
		authType(authType), port(port)
	{}
};
