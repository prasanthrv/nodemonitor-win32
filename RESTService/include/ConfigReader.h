#pragma once
#include "../src/stdafx.h"
#include "../src/pugixml.hpp"
#include "ClientConfig.h"
#include "MailConfig.h"


// Reads xml config file and extracts
// login information and URI for reporting
// see config.xml for reference structure
class ConfigReader
{
public:
	// read xml data and return a map of ClientConfigs
	static unordered_map<string,ClientConfig> readClientData(const string& path);
	static MailConfig readMailConfig(const string& path);
private:
	// load xml with pugi
	static bool loadXML(const string& path, pugi::xml_document& doc) ;
};

