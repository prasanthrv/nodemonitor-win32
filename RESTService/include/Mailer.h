#pragma once
#include "curl\curl.h"

using namespace std;

// Use for mail related activities
namespace Mailer
{
	// send SMTP mail
	bool sendMail(const string& from, const string& pass,
		const string& url, const int port,
		const string& to, const string& authType,
		const string& subject, const string& message);
}


