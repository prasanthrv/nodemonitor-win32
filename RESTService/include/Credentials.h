#pragma once

// represents the client login details
struct Credentials 
{
public:
	const string username;
	const string password;
	const string mail;
	Credentials(string username, string password, string mail) : 
		username(username),
		password(password), mail(mail) {};
	Credentials(const Credentials& cred):
		username(cred.username), password(cred.password),
		mail(cred.mail) {}
};


