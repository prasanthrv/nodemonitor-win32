#pragma once
#include "../src/stdafx.h"

// split a string at a given deliminter
inline vector<string> split_string(string str, const char* tok) {
	vector<string> tokens;

	char* strDup = _strdup(str.c_str());
	size_t strSize = sizeof strDup;
	char* nextToken = NULL;
	char* begin = strtok_s(strDup, tok, &nextToken);
	while (begin) {
		tokens.push_back(string(begin));
		begin = strtok_s(NULL, tok, &nextToken);
	}
	return tokens;
}

// return config.xml path
inline string getConfigPath()
{
	string relPath = "config\\config.xml";
	auto configPath = boost::filesystem::current_path();
	auto dbg = configPath.leaf().filename();
	// current path changes in VS and running from CMD
	// the config file is RESTService\config\config.xml
	if (dbg.string() == "Debug")
		configPath = configPath.parent_path().parent_path();
	configPath /= relPath;
	return configPath.string();
}

