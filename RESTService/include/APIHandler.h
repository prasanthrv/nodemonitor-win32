#pragma once
#include "../src/stdafx.h"

// abstract class representing any handler to an api call
class APIHandler
{
public:
	// process the parameters passed and return (success/failure, error_message)
	virtual pair<bool, string> process(unordered_map<string, string> params) = 0;
};

