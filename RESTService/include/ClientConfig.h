#pragma once
#include "..\src\stdafx.h"
#include "Credentials.h"

// represents a client configuration from the xml file
class ClientConfig
{
public:
	// login details
	const Credentials cDetails;
	// alert details
	const double memoryCapacity;
	const double cpuCapacity;
	const double processCapacity;

	ClientConfig(Credentials, double,double,double);
	ClientConfig(const ClientConfig& client);

	~ClientConfig();
	Credentials credentials();

};

