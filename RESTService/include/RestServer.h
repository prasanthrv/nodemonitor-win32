#include "../src/stdafx.h"
#include "APIHandler.h"
#include <unordered_map>
#include <string>
#include <vector>

using namespace std;

// common http methods
enum class HttpMethod
{
	GET, 
	PUT,
	DEL,
	POST
};




class RestServer final
{
public:
	// create server with (host, path and port)
	RestServer(const UTF8String& host, const UTF8String& relPath, const int port, int duration = 0);
	~RestServer();
	// add an api with appropriate handler
	bool addAPI(const method methodType, const UTF8String& resourceURL, shared_ptr<APIHandler> handler);
	// start running
	void start();
private:
	http_listener listener;

	// dictionaries for (api path, handler)
	unordered_map<UTF8String, shared_ptr<APIHandler>> apiPOST;
	unordered_map<UTF8String, shared_ptr<APIHandler>> apiGET;
	unordered_map<UTF8String, shared_ptr<APIHandler>> apiPUT;
	unordered_map<UTF8String, shared_ptr<APIHandler>> apiDELETE;

	// general method to handle api calls
	void routeHandler(http_request req);
	// parse body of request for parameters
	auto parseBody(const string& body);
	// return appropriate map for each method type (GET,POST etc)
	auto getApiHandlers(const method method);

	// duration to listen
	long duration;

}; 