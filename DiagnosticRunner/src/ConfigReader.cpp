#include "stdafx.h"
#include "pugixml.hpp"
#include "..\include\ConfigReader.h"
#include "..\include\Credentials.h"


bool ConfigReader::loadXML(const string& path, pugi::xml_document& doc) 
{
	pugi::xml_parse_result result = doc.load_file(path.c_str());

	switch (result.status)
	{
		case pugi::xml_parse_status::status_file_not_found:
		{
			string parseErrorMessage = "File " + path + " not found";
			cerr << parseErrorMessage << endl;
			return false;
		}
		case pugi::xml_parse_status::status_ok: 
		{
			return result;
		}
		default:
		{
			string parseErrorMessage = "Error parsing XML file " + path;

			cerr << parseErrorMessage << " Parser result: " << result.status << endl;
			return false;
		}
	}
}

Credentials ConfigReader::readClientCredentials(const string & path) 
{
	pugi::xml_document doc;

	if (loadXML(path, doc))
	{
		// <config>
		auto config = doc.child("config");
		if (config)
		{
			auto lg = config.child("login");
			//<login>
			string login = config.child("login").text().as_string();
			//<password>
			string pass = config.child("password").text().as_string();
			if (login == "" || pass== "")
				throw Errors::DiagError("Cannot parse login config",Errors::error_login);

			Credentials cred(login, pass);
			return cred;
		}
	}
	else
	{
		throw Errors::DiagError("Cannot parse login config",Errors::error_login);
	}
}

 URI ConfigReader::readReportURL(const string & path) 
{
	pugi::xml_document doc;

	if (loadXML(path, doc))
	{
		//<config>
		auto config = doc.child("config");
		if (config)
		{
			//<host>
			string host = config.child("host").text().as_string();
			//<port>
			int port = config.child("port").text().as_int();
			//<path>
			string path = config.child("path").text().as_string();

			if (host == "" || port == 0 || path == "")
				throw Errors::DiagError("Cannot parse login config",Errors::error_login);

			URI uri(host,port,path); 
			return uri;
		}
	}
	else
	{
		throw Errors::DiagError("Cannot parse login config",Errors::error_login);
	}
}

 
