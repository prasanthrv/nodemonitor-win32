#include "..\include\Reporter.h"

#include "cpprest\http_client.h"
#include "cpprest\uri.h"
#include "cpprest\uri_builder.h"


using namespace utility::conversions;
using namespace web::http;

bool Reporter::sendDiagnosis(string login, string pass, string url, 
	long usedMem, long freeMem, 
	double cpuUsage, const string& processDetails)
{
	// build POST query
	stringstream postData;
	postData << "login=" << login << "&pass=" << pass
		<< "&used_mem=" << usedMem << "&free_mem=" << freeMem << "&cpu_usage="
		<< cpuUsage << "&process_details=" << processDetails << endl;

	// create request & client
	client::http_client client(to_utf16string(url));
	http_request request(methods::POST);

	request.set_body(to_utf16string(postData.str()));
	
	// send request
	bool responseOK = false;
	client.request(request)
		.then([&responseOK](http_response response) {
		if (response.status_code() == status_codes::OK)
		{
			responseOK = true;
		}
	}).wait();

	return responseOK;

}


