// DiagnosticRunner.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "../include/DiagnosticRunner.h"
#include "../include/error_codes.h"


void DiagnosticRunner::cleanUp()
{
	if (pLocator != NULL)
		pLocator->Release();
	if (pService != NULL)
		pService->Release();
}

DiagnosticRunner::DiagnosticRunner()
{
	string errMessage;
	// connect to COM
	hRes = CoInitializeEx(NULL, COINIT_MULTITHREADED);
	if (FAILED(hRes))
	{
		errMessage = "Unable to launch COM";
		cerr << errMessage << ": 0x" << std::hex << hRes << endl;
		cleanUp();
		throw Errors::DiagError(errMessage.c_str(), Errors::error_com);
	}

	// get security clearance for WMI Queries
	if ((FAILED(hRes = CoInitializeSecurity(NULL, -1, NULL, NULL, RPC_C_AUTHN_LEVEL_CONNECT, RPC_C_IMP_LEVEL_IMPERSONATE, NULL, EOAC_NONE, 0))))
	{
		errMessage = "Unable to initialize security";
		cerr << errMessage << ": 0x" << std::hex << hRes << endl;
		cleanUp();
		throw Errors::DiagError(errMessage.c_str(), Errors::error_com);
	}

	// get instance of WbemLocator
	if (FAILED(hRes = CoCreateInstance(CLSID_WbemLocator, NULL, CLSCTX_ALL, IID_PPV_ARGS(&pLocator))))
	{
		errMessage = "Unable to create a WbemLocator";
		cerr << errMessage << ": 0x" << std::hex << hRes << endl;
		cleanUp();
		throw Errors::DiagError(errMessage.c_str(), Errors::error_com);
	}

	// connect to CIMV2 namespace
	if (FAILED(hRes = pLocator->ConnectServer(L"root\\CIMV2", NULL, NULL, NULL, WBEM_FLAG_CONNECT_USE_MAX_WAIT, NULL, NULL, &pService)))
	{
		errMessage = "Unable to connect to \"CIMV2\"";
		cerr << errMessage << ": 0x" << std::hex << hRes << endl;
		cleanUp();
		throw Errors::DiagError(errMessage.c_str(), Errors::error_com);
	}
}


DiagnosticRunner::~DiagnosticRunner()
{
	cleanUp();
}

// Queries Win32_OperatingSystem -> (FreePhysicalMemory, TotalVisibleMemorySize)
// for used and free memory
 pair<long,long> DiagnosticRunner::getMemUsage()
{
	
	string errMessage;
	long long freeMem;
	long long totalMem;

	IEnumWbemClassObject* pEnumerator = NULL;
	if (FAILED(hRes = pService->ExecQuery(L"WQL", L"SELECT * FROM Win32_OperatingSystem", WBEM_FLAG_FORWARD_ONLY, NULL, &pEnumerator)))
	{
		cleanUp();
		errMessage = "Unable to retrieve from Win32_OperatingSystem ";
		cout << errMessage << " :0x" << std::hex << hRes << endl;
		throw Errors::DiagError(errMessage.c_str(), Errors::error_com);
	}

	IWbemClassObject* clsObj = NULL;
	int numElems;
	while ((hRes = pEnumerator->Next(WBEM_INFINITE, 1, &clsObj, (ULONG*)&numElems)) != WBEM_S_FALSE)
	{
		if (FAILED(hRes))
			break;

		VARIANT vRet;
		VariantInit(&vRet);
		if (SUCCEEDED(clsObj->Get(L"FreePhysicalMemory", 0, &vRet, NULL, NULL)) && vRet.vt == VT_BSTR)
		{
			_variant_t vt(vRet);
			freeMem = vt;
			VariantClear(&vRet);
		}


		VARIANT vRet2;
		VariantInit(&vRet2);
		if (SUCCEEDED(clsObj->Get(L"TotalVisibleMemorySize", 0, &vRet2, NULL, NULL)) && vRet2.vt == VT_BSTR)
		{
			_variant_t vt(vRet2);
			totalMem = vt;
			VariantClear(&vRet2);
		}

		clsObj->Release();
	}

	long usedMem = totalMem - freeMem;
	usedMem /= 1024; // MB
	freeMem /= 1024; // MB
	return make_pair(usedMem, freeMem);
}

// Queries Win32_Process and enumerates process details
list<ProcessParams> DiagnosticRunner::getProcDetails()
{
	string errMessage;
	int totalProcesses = 0;
	IEnumWbemClassObject* pEnumerator = NULL;

	string name;
	long long memUsage;
	long long cpuUsage;
	list<ProcessParams> procDetails;
	
	if (FAILED(hRes = pService->ExecQuery(L"WQL", L"SELECT * FROM Win32_PerfFormattedData_PerfProc_Process ", 
		WBEM_FLAG_FORWARD_ONLY, NULL, &pEnumerator)))
	{
		cleanUp();
		errMessage = "Unable to retrieve from Win32_PerfFormattedData_PerfProc_Process  ";
		cout << errMessage << " :0x" << std::hex << hRes << endl;
		throw Errors::DiagError(errMessage.c_str(), Errors::error_com);
	}

	IWbemClassObject* clsObj = NULL;
	int numElems;
	while ((hRes = pEnumerator->Next(WBEM_INFINITE, 1, &clsObj, (ULONG*)&numElems)) != WBEM_S_FALSE)
	{
		if (FAILED(hRes))
			break;
		totalProcesses++;

		VARIANT vRet;
		VariantInit(&vRet);
		if (SUCCEEDED(clsObj->Get(L"Name", 0, &vRet, NULL, NULL)) && vRet.vt == VT_BSTR)
		{
			_bstr_t bt(vRet);
			name = bt;
		}

		if (SUCCEEDED(clsObj->Get(L"PercentProcessorTime", 0, &vRet, NULL, NULL)) && vRet.vt == VT_BSTR)
		{
			_variant_t vt(vRet);
			cpuUsage = vt;
		}
		
		if (SUCCEEDED(clsObj->Get(L"PrivateBytes", 0, &vRet, NULL, NULL)) && vRet.vt == VT_BSTR)
		{
			_variant_t vt(vRet);
			memUsage = vt;
			memUsage /= 1024; // KB
			memUsage /= 1024; // MB
		}

		VariantClear(&vRet);
		
		ProcessParams params(name,memUsage,cpuUsage);
		procDetails.push_back(params);
		clsObj->Release();
	}

	return procDetails;
}

string DiagnosticRunner::getProcDetailsAsString()
{
	auto procDetails = getProcDetails();
	stringstream resultStr;

	for (ProcessParams params : procDetails)
		resultStr << params.caption << "," << params.memUsage
			<< "," << params.cpuUsage << ";";

	return resultStr.str();

}
// Queries Win32_PerfFormattedData_PerfOS_Processor -> (PercentProcessorTime) 
// get proc time for all cores and return average
double DiagnosticRunner::getCpuUsage()
{
	string errMessage;
	double cpuUsage = 0.0;
	int count = 0;

	IEnumWbemClassObject* pEnumerator = NULL;
	if (FAILED(hRes = pService->ExecQuery(L"WQL", L"SELECT * FROM Win32_PerfFormattedData_PerfOS_Processor", WBEM_FLAG_FORWARD_ONLY, NULL, &pEnumerator)))
	{
		cleanUp();
		errMessage = "Unable to retrieve from Win32_PerfFormattedData_PerfOS_Processor ";
		cout << errMessage << " :0x" << std::hex << hRes << endl;
		throw Errors::DiagError(errMessage.c_str(), Errors::error_com);
	}

	IWbemClassObject* clsObj = NULL;
	int numElems;
	while ((hRes = pEnumerator->Next(WBEM_INFINITE, 1, &clsObj, (ULONG*)&numElems)) != WBEM_S_FALSE)
	{
		if (FAILED(hRes))
			break;

		VARIANT vRet;
		VariantInit(&vRet);
		if (SUCCEEDED(clsObj->Get(L"PercentProcessorTime", 0, &vRet, NULL, NULL)) && vRet.vt == VT_BSTR)
		{
			count++;
			_variant_t vt(vRet);
			cpuUsage += (int)vt;
			VariantClear(&vRet);
		}

		clsObj->Release();
	}

	return static_cast<double>((cpuUsage/static_cast<double>(count)) / 100.00);
}
