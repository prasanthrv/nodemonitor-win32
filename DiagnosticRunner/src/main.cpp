
#include "stdafx.h"

#include "..\include\ConfigReader.h"
#include "..\include\Reporter.h"
#include "..\include\DiagnosticRunner.h"

#pragma comment(lib, "wbemuuid.lib")
#pragma comment(lib, "comsuppwd.lib")

using namespace std;

int main()
{
	try
	{
		// load login details
		auto configPath = boost::filesystem::current_path();
		auto dbg = configPath.leaf().filename();
		string relPath = "config\\config.xml";
		// current path changes in VS and running from CMD
		// the config file is RESTService\config\config.xml
		if (dbg.string() == "Debug") 
			configPath = configPath.parent_path().parent_path();
		configPath /= relPath;
		Credentials credentials = ConfigReader::readClientCredentials(configPath.string()); 

		//load URL
		URI url = ConfigReader::readReportURL(configPath.string()); 
    
	    // get required system info
	    DiagnosticRunner runner;
	    auto memUsage = runner.getMemUsage();
	    double cpuUsage = runner.getCpuUsage();
		int numProcesses = 0;
		string processDetails = runner.getProcDetailsAsString();

		/*
	    cout << "Used Memory: " << memUsage.first << endl;
	    cout << "Free Memory: " << memUsage.second << endl;
	    cout << "Cpu Usage: " << cpuUsage << endl;
	    cout << "Processes Details: " << processDetails << endl;
		*/

	    //send report
		web::uri_builder builder;
		builder.set_scheme(L"http");
		builder.set_host(to_string_t(url.host), true);
		builder.set_port(url.port);
		builder.set_path(to_string_t(url.path), true);

		// transfer report
	    bool success = Reporter::sendDiagnosis(credentials.username, credentials.password,
	        to_utf8string(builder.to_string()), memUsage.first, memUsage.second, cpuUsage, processDetails);

		if (!success)
			throw Errors::DiagError("Error while transferring", Errors::codes::error_transfer);

	}
	catch (const Errors::DiagError& e)
	{
		cerr << e.what() << endl;
		getchar();
		return e.errCode;
	}
	catch (const exception& e) 
	{
		cerr << e.what() << endl;
		getchar();
		return -1;

	}

	getchar();
	return 0;

}