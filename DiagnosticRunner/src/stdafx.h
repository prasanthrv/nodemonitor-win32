// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <Windows.h>
#include <WbemCli.h>
#include <comutil.h>

#include <unordered_map>
using namespace std;

// TODO: reference additional headers your program requires here
#include "..\include\error_codes.h"
#include "boost\filesystem.hpp"
#include "cpprest\uri_builder.h"
#include "cpprest\uri.h"

using namespace utility::conversions;
using namespace web;