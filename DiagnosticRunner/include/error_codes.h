#pragma once

namespace Errors
{
	// error codes for,
	enum codes
	{
		error_com,      // COM and WMI errors
		error_memory,   // memory usage retreival
		error_cpu,      // cpu usage retreival
		error_proc,     // processes count retreival
		error_login,    // login config retreival
		error_url,      // url config retreival
		error_config,   // xml config retreival
		error_transfer  // rest call error
	};

	// custom exception class to pair an error code and error message
	class DiagError : public exception
	{
		public:
			const int errCode;
			DiagError(const char* what, int errCode) : exception(what), errCode(errCode)
			{}
	};

}
