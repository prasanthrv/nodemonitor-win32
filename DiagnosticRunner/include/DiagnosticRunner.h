#pragma once
#include "stdafx.h"

struct ProcessParams
{
	const string caption;
	const long memUsage;
	const long cpuUsage;
	ProcessParams(string caption, long memUsage, long cpuUsage)
		: caption(caption), memUsage(memUsage), cpuUsage(cpuUsage) {}
};

// This represent a class that collects
// the needed diagnostic information, 
// i.e. mem, cpu utilization and process count
// using WMI. Due to Win32 API, is more C-like in calls
class DiagnosticRunner
{
	// WMI pointers
	HRESULT hRes;
	IWbemLocator* pLocator = NULL;
	IWbemServices* pService = NULL;
	// clean up WMI pointers
	void cleanUp();
public:
	DiagnosticRunner();
	~DiagnosticRunner();

	// get memory usage
	pair<long,long> getMemUsage();
	// get the number of processes
	list<ProcessParams> getProcDetails();
	// get process details as string
	string getProcDetailsAsString();
	// get the cpu usage
	double getCpuUsage();
};

