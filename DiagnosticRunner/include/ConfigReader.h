#pragma once
#include "../src/stdafx.h"
#include "../src/pugixml.hpp"
#include "Credentials.h"

// structure to represent a URI
struct URI
{
public :
	string host;
	int port;
	string path;
	URI(string host, int port, string path) : host(host), port(port), path(path) {}
};

// Reads xml config file and extracts
// login information and URI for reporting
// see config.xml for reference structure
class ConfigReader
{
public:
	// read login information from the xml file
	static Credentials ConfigReader::readClientCredentials(const string & path) ;
	// read uri information from the xml file
	static URI ConfigReader::readReportURL(const string & path) ;

private:
	// private helper function to load the xml
	static bool loadXML(const string& path, pugi::xml_document& doc) ;
	ConfigReader() {}
};

