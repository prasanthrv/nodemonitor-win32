#pragma once

// represents the client login details
struct Credentials 
{
public:
	const string username;
	const string password;
	Credentials(string username, string password) :
		username(username),
		password(password) {};
};


