#pragma once
#include "..\src\stdafx.h"
// Class to send reports to API
class Reporter
{
public:
	// send the required diagnostic message to the server
	static bool sendDiagnosis(string login, string pass, string url, 
		long usedMem, long freeMem, 
		double cpuUsage, const string& processDetails);
};

